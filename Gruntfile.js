var mozjpeg = require('imagemin-mozjpeg');
var path = 'app';

module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        
        sass: {
            dev: {
                options: {
                    outputStyle: 'expanded'
                },
                files: {
                    'app/dist/css/app.css': path + '/styles/scss/app.scss'
                }
            },
            prod: {
                options: {
                    outputStyle: 'compressed',
                },
                files: {
                    'app/dist/css/app.min.css': path + '/styles/scss/app.scss'
                }
            }
        },

        postcss: {
            options: {
                processors: [
                    require('autoprefixer')({browsers: ['ie <= 8', 'last 2 versions']})
                ]
            },
            all: { src: path + '/dist/css/*.css' }
        },

        concat: {
            general: {
                src: [
                    path + '/scripts/vendor/jquery.js',
                    path + '/scripts/utilities.js',
                    path + '/scripts/main.js'
                ],
                dest: path + '/dist/scripts/general.js'
            },
        },

        uglify: {
            build: {
                files: {
                    'app/dist/scripts/modernizr.js': [path + '/scripts/vendor/modernizr-custom.js'],
                    'app/dist/scripts/general.min.js': [path + '/dist/scripts/general.js']
                }
            }
        },

        watch: {
            options: {
                livereload: false
            },
            scripts: {
                files: [path + '/scripts/**/*.js', path + '/scripts/*.js'],
                tasks: ['concat', 'notify:concat', 'uglify', 'notify:uglify'],
                options: {
                    spawn: false,
                }
            },
            sass: {
                files: [path + '/styles/scss/*', path + '/styles/scss/**/*.scss'],
                tasks: ['sass:dev', 'notify:sassdev', 'sass:prod', 'notify:sassprod', 'postcss:all', 'notify:postcss'],
                options: {
                    spawn: false,
                }
            }
        },
        
        notify : {
            concat : {
                options: {
                    title : "Grunt JS",
                    message : "js concatinated"
                }
            },
            uglify : {
                options: {
                    title : "Grunt JS",
                    message : "js minified"
                }
            },
            sassdev : {
                options: {
                    title : "Grunt Sass",
                    message : "dev css created"
                }
            },
            sassprod : {
                options: {
                    title : "Grunt Sass",
                    message : "prod css created"
                }
            },
            postcss : {
                options: {
                    title : "Grunt PostCSS",
                    message : "css prefixed"
                }
            }
        }

    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-bower-task');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-contrib-rename');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-newer');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['watch']);

};