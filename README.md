#Open the gates for data: app
------------------------------
Auteurs:

**Mathias Van Kerckvoorde**  

* GitHub Account: https://github.com/mathvank1994/ 
* Bitbucket Account: https://bitbucket.org/mathvank/   
* Email: mathias.vankerckvoorde@student.arteveldehs.be

**Dylan Van Steirteghem**

* GitHub Account: https://github.com/dylavans/ 
* Bitbucket Account: https://bitbucket.org/dylavans/  
* Email: dylan.vansteirteghem@student.arteveldehs.be