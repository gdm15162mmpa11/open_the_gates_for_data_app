// Anonymous function executed when document loaded
$(document).ready(function(){

	// Describe an App object with own functionalities
	var App = {

		init: function() {

			var self = this;

			// API URLS
			this.countriesURL = "http://api.worldbank.org/countries/all?format=jsonP&prefix=jsonp_callback_br&per_page=300";

			this.target1aURL = "http://api.worldbank.org/countries/{0}/indicators/SI.POV.GAPS?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";
			this.target1bURL = "http://api.worldbank.org/countries/{0}/indicators/SL.GDP.PCAP.EM.KD?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";
			this.target1cURL = "http://api.worldbank.org/countries/{0}/indicators/SH.STA.MALN.ZS?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";

			this.target2aURL = "http://api.worldbank.org/countries/{0}/indicators/SE.PRM.TENR?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";

			this.target3aURL = "http://api.worldbank.org/countries/{0}/indicators/SE.ENR.PRIM.FM.ZS?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";

			this.target4aURL = "http://api.worldbank.org/countries/{0}/indicators/SP.DYN.IMRT.IN?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";

			this.target5aURL = "http://api.worldbank.org/countries/{0}/indicators/SH.STA.MMRT?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";
			this.target5bURL = "http://api.worldbank.org/countries/{0}/indicators/SH.STA.ANVC.ZS?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";

			this.target6aURL = "http://api.worldbank.org/countries/{0}/indicators/SH.DYN.AIDS.ZS?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";
			this.target6bURL = "http://api.worldbank.org/countries/{0}/indicators/SH.HIV.ARTC.ZS?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";
			this.target6cURL = "http://api.worldbank.org/countries/{0}/indicators/SH.TBS.INCD?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";

			this.target7aURL = "http://api.worldbank.org/countries/{0}/indicators/EN.ATM.CO2E.KT?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";
			this.target7bURL = "http://api.worldbank.org/countries/{0}/indicators/ER.LND.PTLD.ZS?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";
			this.target7cURL = "http://api.worldbank.org/countries/{0}/indicators/SH.H2O.SAFE.ZS?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";
			this.target7dURL = "http://api.worldbank.org/countries/{0}/indicators/EN.POP.SLUM.UR.ZS?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";

			this.target8aURL = "http://api.worldbank.org/countries/{0}/indicators/DC.ODA.TOTL.CD?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";
			this.target8bURL = "http://api.worldbank.org/countries/{0}/indicators/DC.ODA.TLDC.CD?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";
			this.target8dURL = "http://api.worldbank.org/countries/{0}/indicators/DT.TDS.DECT.EX.ZS?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";
			this.target8fURL = "http://api.worldbank.org/countries/{0}/indicators/IT.NET.USER.P2?per_page=100&date=1990:2015&format=jsonP&prefix=json_callback_br";

			this.registerNavigationToggleListeners(); // Register All Navigation Toggle Listeners
			this.registerWindowListeners(); // Register All Navigation Toggle Listeners

			// This function will fill up our country select
			this.buildSelect();
		},

		buildSelect: function(){
			// If there is a wrapper on the page
			if($('.select-wrapper').length > 0){
				// Get all countries from API
				Utils.getJSONPByPromise(this.countriesURL).then(
					function(data){

						// Build HTML
						var outerHTML = '<select><option value="">Please choose a country..</option>';

						_.each(data[1], function(item){
							outerHTML += "<option value="+item.iso2Code+">" + item.name + "</option>";
						});

						outerHTML += '</select>';

						// Add our generated HTML to the wrapper
						$('.select-wrapper').append(outerHTML);

					},function(status) {
						console.log(status);
				});
			}
		},

    	drawChartForSelection: function(apiURL, country){    		    		

    		// Build URL for subject and country (replace {0})
    		var url = String.format(apiURL , country );
    		
    		Utils.getJSONPByPromise(url).then(
				function(data){

					// If there is a data response, count if atleast one value is not empty
					if(data[1]){
						var counter = 0;
						var total_results = data[1].length;
						_.each(data[1], function(item){
							if(item.value === null){
								counter++;
							}
						});
					}
					
					// If there is a data response AND atleast one value is not empty
					if(counter != total_results && data[1]){					

						// Initialize arrays
						var labels = [];
						var series = [];

						// Loop over data[1] - data[0] is some pagination/meta data we don't need
						_.each(data[1].reverse(), function(item) {
								// Save label and use the last 2 numbers to save some room in our graph
								var label = item.date;
								labels.push( label.slice(-2) );

								// Save value so we can a draw point
								series.push(parseFloat(item.value));	
						});

						// Calculate high and lows, so our graph has an optimized view
						var options = {
							low: _.min(_.pluck(data[1], 'value')),
							hight: _.max(_.pluck(data[1], 'value'))
						};

						// In adition to the regular options we specify responsive option overrides that will override the default configutation based on the matching media queries
						var responsiveOptions = [
						   
						  ['screen and (max-width: 640px)', {
						    showLine: false,
						    axisX: {
						      labelInterpolationFnc: function(value) {
						        return;
						      }
						    }
						  }]
						];

						// Our data object containing labels and series
						var data = {
							labels: labels,
							series: [series]
						};

						// Empty the container (you never known)
						$(".chartist-line").empty();

						// Create a new line chart object where as first parameter we pass in a selector that is resolving to our chart container element
						// The second parameter is the actual data object
						// Third parameter are the responsive options
						var chart = new Chartist.Line('.chartist-line', data, options, responsiveOptions);

						// Let's put a sequence number aside so we can use it in the event callbacks
						var seq = 0,
						  delays = 75,
						  durations = 75;

						// Once the chart is fully created we reset the sequence
						chart.on('created', function() {
						  seq = 0;
						});

						chart.on('draw', function(data) {
							  seq++;

							  if(data.type === 'line') {
							    // If the drawn element is a line we do a simple opacity fade in. This could also be achieved using CSS3 animations.
							    data.element.animate({
							      opacity: {
							        // The delay when we like to start the animation
							        begin: seq * delays + 1000,
							        // Duration of the animation
							        dur: durations,
							        // The value where the animation should start
							        from: 0,
							        // The value where it should end
							        to: 1
							      }
							    });
							  } else if(data.type === 'label' && data.axis === 'x') {
							    data.element.animate({
							      y: {
							        begin: seq * delays,
							        dur: durations,
							        from: data.y + 100,
							        to: data.y,
							        // We can specify an easing function from Chartist.Svg.Easing
							        easing: 'easeOutQuart'
							      }
							    });
							  } else if(data.type === 'label' && data.axis === 'y') {
							    data.element.animate({
							      x: {
							        begin: seq * delays,
							        dur: durations,
							        from: data.x - 100,
							        to: data.x,
							        easing: 'easeOutQuart'
							      }
							    });
							  } else if(data.type === 'point') {
							    data.element.animate({
							      x1: {
							        begin: seq * delays,
							        dur: durations,
							        from: data.x - 10,
							        to: data.x,
							        easing: 'easeOutQuart'
							      },
							      x2: {
							        begin: seq * delays,
							        dur: durations,
							        from: data.x - 10,
							        to: data.x,
							        easing: 'easeOutQuart'
							      },
							      opacity: {
							        begin: seq * delays,
							        dur: durations,
							        from: 0,
							        to: 1,
							        easing: 'easeOutQuart'
							      }
							    });
							  } else if(data.type === 'grid') {
							    // Using data.axis we get x or y which we can use to construct our animation definition objects
							    var pos1Animation = {
							      begin: seq * delays,
							      dur: durations,
							      from: data[data.axis.units.pos + '1'] - 30,
							      to: data[data.axis.units.pos + '1'],
							      easing: 'easeOutQuart'
							    };

							    var pos2Animation = {
							      begin: seq * delays,
							      dur: durations,
							      from: data[data.axis.units.pos + '2'] - 100,
							      to: data[data.axis.units.pos + '2'],
							      easing: 'easeOutQuart'
							    };

							    var animations = {};
							    animations[data.axis.units.pos + '1'] = pos1Animation;
							    animations[data.axis.units.pos + '2'] = pos2Animation;
							    animations['opacity'] = {
							      begin: seq * delays,
							      dur: durations,
							      from: 0,
							      to: 1,
							      easing: 'easeOutQuart'
							    };

							    data.element.animate(animations);
							  }
							});

					} else {
						// Empty the container
						$(".chartist-line").empty();
						// Show fallback when no data
						$(".chartist-line").append("<p>There is no data for this country. Please try another one.</p>");
					}

					//console.log('success');

				},function(status) {
					console.log(status);
			});
    	},
		registerNavigationToggleListeners: function() {
			// Some helpers
			var toggles = document.querySelectorAll('.navigation-toggle');

			if(toggles != null && toggles.length > 0) {
				var toggle = null;

				for(var i = 0; i < toggles.length; i++ ) {
					toggle = toggles[i];
					toggle.addEventListener('click', function(ev) {
						ev.preventDefault();

						document.querySelector('body').classList.toggle(this.dataset.navtype);

						return false;
					});
				}
			}
		},
		
		registerWindowListeners: function() {
			var self = this;
			$('.select-wrapper').on('change', 'select', function(){

				// Watch when our country select changes and take this data-subject attribute, use this value in a switch to decide which URL and country we need to show
				switch($(this).parent().attr('data-subject') ){

					case 'target1a':{
						self.drawChartForSelection(self.target1aURL, $(this).val() );
						break;
					}
					case 'target1b':{
						self.drawChartForSelection(self.target1bURL, $(this).val() );
						break;
					}
					case 'target1c':{
						self.drawChartForSelection(self.target1cURL, $(this).val() );
						break;
					}

					case 'target2a':{
						self.drawChartForSelection(self.target2aURL, $(this).val() );
						break;
					}

					case 'target3a':{
						self.drawChartForSelection(self.target3aURL, $(this).val() );
						break;
					}

					case 'target4a':{
						self.drawChartForSelection(self.target4aURL, $(this).val() );
						break;
					}

					case 'target5a':{
						self.drawChartForSelection(self.target5aURL, $(this).val() );
						break;
					}
					case 'target5b':{
						self.drawChartForSelection(self.target5bURL, $(this).val() );
						break;
					}

					case 'target6a':{
						self.drawChartForSelection(self.target6aURL, $(this).val() );
						break;
					}
					case 'target6b':{
						self.drawChartForSelection(self.target6bURL, $(this).val() );
						break;
					}
					case 'target6c':{
						self.drawChartForSelection(self.target6cURL, $(this).val() );
						break;
					}

					case 'target7a':{
						self.drawChartForSelection(self.target7aURL, $(this).val() );
						break;
					}
					case 'target7b':{
						self.drawChartForSelection(self.target7bURL, $(this).val() );
						break;
					}
					case 'target7c':{
						self.drawChartForSelection(self.target7cURL, $(this).val() );
						break;
					}
					case 'target7d':{
						self.drawChartForSelection(self.target7dURL, $(this).val() );
						break;
					}

					case 'target8a':{
						self.drawChartForSelection(self.target8aURL, $(this).val() );
						break;
					}
					case 'target8b':{
						self.drawChartForSelection(self.target8bURL, $(this).val() );
						break;
					}
					case 'target8d':{
						self.drawChartForSelection(self.target8dURL, $(this).val() );
						break;
					}
					case 'target8f':{
						self.drawChartForSelection(self.target8fURL, $(this).val() );
						break;
					}
				}
			});
	
			// Some helpers
			window.addEventListener('resize', function(ev) {
				if(document.querySelector('body').classList.contains('offcanvas-open')) {
					document.querySelector('body').classList.remove('offcanvas-open');
				}

				if(document.querySelector('body').classList.contains('headernav-open')) {
					document.querySelector('body').classList.remove('headernav-open');
				}
			});
		}
	};

	App.init(); // Intialize the application

});